<?php

namespace App\Http\Controllers;

use App\Models\sinhvien;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class SinhvienController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sinhvien = sinhvien::paginate(5); //fetch all products from DB
        return view('index', compact('sinhvien'))->with('i',(request()->input('page',1)-1)*5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        sinhvien::create($request->all());
        return redirect()->route('sinhvien.index')->with('thongbao','them sinh vien thanh cong !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\sinhvien  $sinhvien
     * @return \Illuminate\Http\Response
     */
    public function search($id)
    {
        $sinhvien=DB::table('sinhviens')->where('id',$id)->get();
        return view('search',compact('sinhvien'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sinhvien  $sinhvien
     * @return \Illuminate\Http\Response
     */
    public function edit(sinhvien $sinhvien)
    {
        return view('edit', compact('sinhvien'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sinhvien  $sinhvien
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sinhvien $sinhvien)
    {
        // $sinhvien->update([
        //     'fullname' => $request->fullname,
        //     'birthday' => $request->birthday,
        //     'address' => $request->address
        // ]);
        
        // return redirect('sinhvien.index' . $sinhvien->id . 'sinhvien.edit');
        $sinhvien->update($request->all());
        return redirect()->route('sinhvien.index')->with('thongbao','cap nhat thanh cong !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\sinhvien  $sinhvien
     * @return \Illuminate\Http\Response
     */
    public function destroy(sinhvien $sinhvien)
    {
        $sinhvien->delete();
        return redirect()->route('sinhvien.index')->with('thongbao','xoa thanh cong !');
    }
}