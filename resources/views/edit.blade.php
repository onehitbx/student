@extends('layout')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                        <h3>Sửa thông tin sinh viên</h3>
                    </div>
                    <div class="col-md-6">
                        <a href="{{route('sinhvien.index')}}" class="btn btn-primary float-end">Danh sách sinh viên</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{route('sinhvien.update',$sinhvien->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <strong>Full name</strong>
                                <input type="text" name="fullname" value="{{$sinhvien->fullname}}" class="form-control" placeholder="nhập họ tên sinh viên">
                            </div>
                            <div class="form-group">
                                <strong>Birthday</strong>
                                <input type="date" name="birthday" value="{{$sinhvien->birthday}}" class="form-control" placeholder="nhập ngày sinh">
                            </div>
                            <div class="form-group">
                                <strong>Address</strong>
                                <input type="text" name="address" value="{{$sinhvien->address}}" class="form-control" placeholder="nhập địa chỉ">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success mt-2">Cập nhật</button>
                </form>               
            </div>
        </div>
    </div>
@endsection