@extends('layout')
@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-md-6">
                    <h3>Quản lý sinh viên</h3>
                </div> 
                <div class="col-md-6">
                    <a href="{{route('sinhvien.create')}}" class="btn btn-primary float-end">Thêm mới</a>
                </div>
                {{-- <div class="col-md-6">
                    <a href="{{route('sinhvien.search')}}" class="btn btn-primary float-end">Tìm kiếm</a>
                </div> --}}
            </div>
        </div>
        <div class="card-body">
            @if (Session::has('thongbao'))
            <div class="alert alert-success">
                {{Session::get('thongbao')}}
            </div>             
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Full Name</th>
                        <th>Birthday</th>
                        <th>Address</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($sinhvien as $sv)
                        <tr>
                            <td>{{++$i}}</td>
                            <td>{{$sv->fullname}}</td>
                            <td>{{$sv->birthday}}</td>
                            <td>{{$sv->address}}</td>
                            <td>
                                <form action="{{route('sinhvien.destroy',$sv->id)}}" method="POST" class="text-center">
                                    <a href="{{route('sinhvien.edit',$sv->id)}}" class="btn btn-info">Sửa</a>
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Xóa</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>